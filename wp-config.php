<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp.dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CW+RD_GDeZF#mjs#p8mcB^V:/:YN7BQ`XqK>eFD-}3 kf$xbr|*fPc~fizt?l47&');
define('SECURE_AUTH_KEY',  '?p<WlL,xAZTk9`r6wct6n3Mt]@jO>>F9ZMzEGh) hDoqmsQ>t]+WJoEAsAgQqaKj');
define('LOGGED_IN_KEY',    'd1h9Wb,cFOpolh`%`z2SKeAgH!QL^/DSeb]!Ao{LPa,%_@.$IHY?6]9,50s<P|cB');
define('NONCE_KEY',        'M;]Sp3:*Lx{M*7r-r8R!ZfT/S6$@!bxDaUX75.,jyv*~~PvU5h~E!JHV%m,_~h@n');
define('AUTH_SALT',        ':>qZyP?8gY2&V6<-)N|BmjNi:3BpD1q6)AZ}em]|Nww19ZkeG[]}8bZnb1qlY=@c');
define('SECURE_AUTH_SALT', 'qbsp4Ld2+]c$g*<=a^!oieEK MTeTd~+[^P7H|`E!KKMuOsBF&-u#2kzl(qnl!|K');
define('LOGGED_IN_SALT',   'xR^QGE}7AH?jd]iWZbWyNlYcU&`rysDI*?,alM5@SL#079t7or ZVmxZFYadsAK?');
define('NONCE_SALT',       '?)p;7[ h&9oj $kG9*4(j?9S3p4iNW)[WV>)MXdtFB$pq4(9(E$Sek48U6<q45)P');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
